package src;

import com.sun.org.apache.bcel.internal.generic.NEW;

import controller.Controller;
import junit.framework.TestCase;
import model.data_structures.ListaDoble;
import model.vo.Taxi;
import view.TaxiTripsManagerView;

public class ListaDobleTest extends TestCase
{
	//---------------------------------------------------------
	// ATRIBUTOS
	//--------------------------------------------------------
	Controller controlador = new Controller();
	ListaDoble <Taxi> taxis;
	
	//--------------------------------------------------------
	// ESCENARIOS
	//--------------------------------------------------------
	
	/**
	 * Escenario vac�o.
	 */
	private void setupEscenario1()
	{
		
		taxis = new ListaDoble<Taxi>();
	}
	
	/**
	 * Escenario con algunos elementos
	 */
	private void setupEscenario2()
	{
		setupEscenario1();
		Taxi prueba0 = new Taxi("Prueba1", "Independiente");
		Taxi prueba1 = new Taxi("Prueba2", "Independiente");
		Taxi prueba2 = new Taxi("Prueba3", "Independiente");
		Taxi prueba3 = new Taxi("Prueba2", "Independiente");
		taxis.a�adir(prueba0);
		taxis.a�adir(prueba1);
		taxis.a�adir(prueba2);
		taxis.a�adir(prueba3);
	}
	
	/**
	 * Escenario con la lista de taxis independientes
	 */
	private void setupEscenario3()
	{
		controlador.loadServices();
		taxis = controlador.getTaxisOfCompany("Independiente");
	}
	
	//----------------------------------------------------------------------
	// TESTS
	//----------------------------------------------------------------------
	
	/**
	 * Tests que se encarga de probar el m�todo dar elementos
	 */
	public void testDarElementos()
	{
		//Cargar el esenario con pocos elementos
		setupEscenario2();
		Taxi prueba1 = new Taxi ("Prueba1", "TaxiLibre");
		//Crear y a�adir un elemento a la lista de pocos elementos (Queda A�adido en la �ltima posicion)
		taxis.a�adir(prueba1);
		//Buscamos la �ltima posici�n del arreglo
		assertEquals(prueba1, taxis.dar(4));
		// Cargamos el escenarios con todos los taxis "Independientes"
		setupEscenario3();
		// Pedimos el numero de elementos que tiene la lista
		int numeroTaxis = taxis.darTama�o();
		// A�adimos el taxi creeado en la primera parte del test
		taxis.a�adir(prueba1);
		// Pedimos que el elemento a�adido y la posicion ultima sean iguales.
		// Recordar que el tama�o siempre es uno mayor a la posicion (Contando desde 0)		
		assertEquals(prueba1, taxis.dar(numeroTaxis));
		// A�adimos de primero
		taxis.a�adirPrimero(prueba1);
		// Corroboramos con la posici�n 0 (La primera)
		assertEquals(prueba1, taxis.dar(0));
	}
	
	/**
	 * Test que prueba el agregar elementos al final
	 */
	public void testDarTama�o ()
	{
		// Probamos con el escenario 1 con el que nos deben dar 0 elementos
		setupEscenario1();
		assertEquals(0,taxis.darTama�o());
		// Probamos con el escenario 2 con el que nos deben dar 4 elementos. ( En el escenario 2
		// se agregan los elementos
		setupEscenario2();
		assertEquals(4, taxis.darTama�o());
		// Probamos con el escenario 3 en que se debe dar 936 elementos
		// 936 son los taxis que son independientes.
		setupEscenario3();
		assertEquals(936, taxis.darTama�o());
	}
	
	/**
	 * Test que se encarga de probar los m�todos a�adirPrimero(), darPrimero(), a�adirUltimo(), darUltimo()
	 */
	public void testAgregarYDarPrimeroYAgregarYDarUltimo()
	{
		// Cargo los esenarios con pocos taxis
		setupEscenario1();
		setupEscenario2();
		// Creo un taxi
		Taxi prueba1 = new Taxi ("Prueba1", "TaxiLibre");
		//lo agrego de primero y compruebo que sea igual al de la primera posici�n)
		taxis.a�adirPrimero(prueba1);
		assertEquals(prueba1, taxis.darPrimero().darItem());
		// Lo agrego de ultimo y compruebo que sea igual al de la �ltima posici�n 
		taxis.a�adirUltimo(prueba1);
		assertEquals(prueba1, taxis.darUltimo().darItem());
	}
	
	/**
	 * Tets que se encarga de probar el m�todo eliminar de la lista
	 */
	public void testEliminar()
	{
		// Cargo los escenarios de pocos datos
		setupEscenario1();
		setupEscenario2();
		// Pido un taxi en la posicion x y lo apunto con una variable hago lo mismo con x+1
		Taxi prueba1 = (Taxi) taxis.dar(2);
		Taxi nuevoX = (Taxi) taxis.dar(3);
		// Elimino el taxi x y compruebo que el elemto que hay en la posicion 
		taxis.eliminar(prueba1);
		assertEquals(nuevoX, taxis.dar(2));
		
		
	}
	
	
	
	
	

}
