package controller;

import api.ITaxiTripsManager;
import model.data_structures.LinkedList;
import model.data_structures.ListaDoble;
import model.logic.TaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static TaxiTripsManager  manager = new TaxiTripsManager();
	
	public static void loadServices( ) {
		// To define the dataset file's name 
		String serviceFile = "./data/taxi-trips-wrvz-psew-subset-small.json";
		manager.loadServices( serviceFile );
	}
		
	public static ListaDoble<Taxi> getTaxisOfCompany (String company) {
		return manager.getTaxisOfCompany(company);
	}
	
	public static ListaDoble<Service> getTaxiServicesToCommunityArea(int communityArea) {
		return manager.getTaxiServicesToCommunityArea( communityArea );
	}
}
