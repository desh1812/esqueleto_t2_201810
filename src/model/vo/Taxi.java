package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	//--------------------------------------------------------------------------------
	// ATRIBUTOS
	//-------------------------------------------------------------------------------
	
	/**
	 * M�todo que representa el id del taxi
	 */
	String taxiId;
	
	/**
	 * M�todo que representa la compa��a de taxi
	 */
	String compa�ia;
	
	//---------------------------------------------------------------------------------
	// CONSTRUCTOR
	//---------------------------------------------------------------------------------
	public Taxi(String pId, String pCompa�ia)
	{
		taxiId = pId;
		compa�ia = pCompa�ia;
	}
	//---------------------------------------------------------------------------------
	// M�TODOS
	//---------------------------------------------------------------------------------
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		// TODO Auto-generated method stub
		return taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return compa�ia;
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return 0;
	}	
}
