package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the firt element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface LinkedList <T extends Comparable<T>>  
{
	/**
	 * M�todo que a�ade un item en la primera posici�n	
	 * @param item Item a a�adir
	 */
	public void a�adirPrimero(T item);
	/**
	 * A�ande el elmento a una posici�n de la lista
	 * @param item Item a a�adir
	 */
	public void a�adir(T item);
	/**
	 * Elimina el primer elemento
	 */
	public void eliminarPrimero();
	/**
	 * Elimina un elemento de la lista dada por par�metro
	 * @param item elemento a eliminar
	 */
	public void eliminar(T item);
	/**
	 * Retorna el elemento dado por parametro
	 * @param T elemento a eliminar
	 * @return el elemento eliminado
	 */
	public T dar (int numero);
	/**
	 * Retorna el tama�ano de lista 
	 * @return Tama�o de la lista
	 */
	public int darTama�o();
	/**
	 * Retorna si la lista est� vac�a o no
	 * @return True si la lista est� Vac�a y False si tiene alg�n elemento
	 */
	public boolean estaVacia();
	
	
}
