package model.data_structures;

public class ListaDoble <T extends Comparable<T>> implements LinkedList{

	//-----------------------------------------------------------------
		// ATRIBUTOS
		//-----------------------------------------------------------------
		/**
		 * Nodo que representa la cabeza de la lista
		 */
		private Node<T> cabeza;
		/**
		 * Nodo que representa el fin de la lista
		 */
		private Node<T> fin;
		/**
		 * Numero de elementos que posee la lista
		 */
		private int tamaño = 0;
		
		//-----------------------------------------------------------------
		// CONSTRUCTOR
		//----------------------------------------------------------------
		public ListaDoble( ) 
		{
			cabeza = new Node<>(null, null, null);
			fin = new Node<>(null, cabeza, null);
			cabeza.cambiarSiguiente(fin); 
		}
		
		//-----------------------------------------------------------------
		// MÉTODOS
		//-----------------------------------------------------------------
		
		/**
		 * Retorna el primer nodo
		 * @return
		 */
		public Node<T> darPrimero()
		{
			if( estaVacia() )
				return null;
			else
				return cabeza.darSiguiente();
				
		}
		
		/**
		 * Método que retorna el último elemento de la lista.
		 * @return
		 */
		public Node<T> darUltimo()
		{
			if ( estaVacia() )
				return null;
			else
				return fin.darAnterior();
		}
		/**
		 * Método que devuelve el tamaño del arreglo
		 */
		public int darTamaño() 
		{
			return tamaño;
		}

		/**
		 * Método que verifica si la lista está vacia o no
		 */
		public boolean estaVacia() 
		{
			if( tamaño == 0 )
				return true;
			else
				return false;
		}
		/**
		 * Método que se encarga de añadir un elemento a la lista en la primera posición
		 * @param item. Item a añadir a la lista.
		 */
		public void añadirPrimero ( Comparable  item ) 
		{
			añadirEntre( (T) item , cabeza, cabeza.darSiguiente());
		}
		/**
		 * Método que se encarga de añadir un elemento a la lista en la ultima posisión.
		 * @param item. Item a añadir a la lista.
		 */
		public void añadirUltimo ( Comparable item )
		{
			añadirEntre ( (T) item , fin.darAnterior(), fin);
		}
		/**
		 * Método que se encarga de añadir un elemento entre dos nodos dados por parametros. 
		 * @param item. Item a añadir a lista
		 * @param predecesor. Nodo predecesor al que se quiere añadir.
		 * @param sucesor. Nodo sucesor al que se quiere añadir.
		 */
		public void añadirEntre(T item, Node<T> predecesor, Node<T> sucesor)
		{
			Node<T> nuevo = new Node<T> ( item, predecesor, sucesor);
			predecesor.cambiarSiguiente(nuevo);
			sucesor.cambiarAnterior(nuevo);
			tamaño ++;
		}
		

		
		public void eliminarPrimero() {
			
			eliminar(cabeza.darSiguiente().darItem());
		}

		
		public void eliminar(Comparable item) 
		{
			boolean termine = false;
			Node <T> nodo = darPrimero();
			while ( !termine &&nodo.darSiguiente() != fin )
			{
				if( nodo.darItem().equals(item))
				{
					termine = true;
				}
				else
					nodo= nodo.darSiguiente();
			}
			System.out.println(nodo.darItem());
			Node<T> predecesor = nodo.darAnterior();
			Node<T> sucesor = nodo.darSiguiente();
			predecesor.cambiarSiguiente(sucesor);
			sucesor.cambiarAnterior(predecesor);
			nodo.cambiarSiguiente(null);
			nodo.cambiarAnterior(null);
			tamaño--;
					
		}

		
		public Comparable dar(int numero) 
		{
			Node <T> nodo = darPrimero();
			if(darTamaño()>numero)
			{
				while ( numero != 0 )
				{
					nodo = nodo.darSiguiente();
					numero--;
				}
				return nodo.darItem();
			}
			else
			return null;
		}
		
		
		public void añadir(Comparable item) {
			if(estaVacia())
			{
				añadirPrimero(item);
			}
			else
				añadirUltimo(item);
			
		}



	
	

	

}
