package model.data_structures;

public class Node <E > 
{
	
	//------------------------------------------------------------
	// ATRIBUTOS
	//------------------------------------------------------------
	/**
	 * Nodo siguiente
	 */
	private Node<E> siguiente;
	/**
	 * Nodo anterior
	 */
	private Node<E> anterior;
	/**
	 * Item con la informaci�n del nodo
	 */
	private E item;
	
	//-------------------------------------------------------------
	// CONSTRUCTURES
	//-------------------------------------------------------------
	/**
	 * Constructor 
	 * @param item Objeto el cual vamos a usar
	 */
	public Node (E item, Node<E> pAnterior, Node<E> pSiguiente) 
	{
	siguiente =  pSiguiente;
	anterior = pAnterior;
	this.item = item;
	}
	/**
	 * M�todo por el cual devuelvo el siguiente de la lista
	 * @return Nodo siguiente
	 */
	public Node<E> darSiguiente() 
	{
	return siguiente;
	}
	/**
	 * M�todo que devuelve el anterior de la lista
	 * @return Nodo anterior
	 */
	public Node<E> darAnterior()
	{
		return anterior;
	}
	/**
	 * M�todo que inserta en la posici�n siguiente.
	 * @param next
	 */
	public void cambiarSiguiente ( Node<E> siguiente ) 
	{
		this.siguiente = siguiente ;
	}
	/**
	 * M�to que inserta un nodo en la posici�n anterior.
	 */
	public void cambiarAnterior ( Node<E> anterior )
	{
		this.anterior = anterior;		
	}
	/**
	 * Retorna el item que contiene el ndo
	 * @return item que contiene el nodo
	 */
	public E darItem()
	{
		return item;
	}
	/**
	 * Inserta el item en el nodo
	 * @param item. Item a insertar
	 */
	public void cambiarItem (E item) 
	{
		this.item = item;
	}
}
