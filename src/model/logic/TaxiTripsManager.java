  package model.logic;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;




import api.ITaxiTripsManager;

import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.ListaDoble;
import model.data_structures.Node;

public class TaxiTripsManager<E> implements ITaxiTripsManager {

	//------------------------------------------------------------------
		// ATRIBUTOS
		//-----------------------------------------------------------------
		/**
		 * Atributo que representa la lista de taxis
		 */
		ListaDoble taxis;
		
		/**
		 * Atributo que representa la lista de los servicios
		 */
		ListaDoble servicios;
		
		/**
		 * M�todo que se encarda de cargar los servicios
		 */
		public void loadServices(String serviceFile) {
			JSONParser parser = new JSONParser();
			taxis = new ListaDoble <Taxi>();
			servicios = new ListaDoble <Service>();
			//"dropoff_census_tract":"17031839100","dropoff_centroid_latitude":"41.880994471","dropoff_centroid_location":{"type":"Point","coordinates":[-87.6327464887,41.8809944707]},"dropoff_centroid_longitude":"-87.632746489","dropoff_community_area":"32","extras":"0","fare":"6","payment_type":"Credit Card","pickup_census_tract":"17031320100","pickup_centroid_latitude":"41.884987192","pickup_centroid_location":{"type":"Point","coordinates":[-87.6209929134,41.8849871918]},"pickup_centroid_longitude":"-87.620992913","pickup_community_area":"32","taxi_id":"7ed122481c0964a5555309bf4696e25bbf7def086d7ecb94b4910b6129501468f748621f8c51a9e0daaaa8ecca9624925356a53e2860cefdbc3caf730404d9a9","tips":"2","tolls":"0","trip_end_timestamp":"2017-02-01T09:00:00.000","trip_id":"8b6010e14e1be89b90d9c51f9e8d2be14d162a2c","trip_miles":"0","trip_seconds":"300","trip_start_timestamp":"2017-02-01T09:00:00.000","trip_total":"8"}
			try
			{
				String taxiTripsDatos = "./data/taxi-trips-wrvz-psew-subset-small.json";
				
				/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
				JSONArray arr= (JSONArray) parser.parse(new FileReader(taxiTripsDatos));
				Iterator iter = arr.iterator();
				int z = 0;
				while ( iter.hasNext() )
				{
					JSONObject objeto = (JSONObject)arr.get(z);
					String company = "Independiente";
					if(objeto.get("company") != null)
					{
						company = (String) objeto.get("company");
					}
					String dropoffCensusTract = "000000000000";
					if(objeto.get("dropoff_census_tract") != null)
					{
						dropoffCensusTract = (String) objeto.get("dropoff_census_tract");
					}
					String dropoffCentroidLatitude = (String)objeto.get("dropoff_centroid_latitude");
					ArrayList dropoffCentroidLocationA = new ArrayList(); 
					if(objeto.get("dropoff_centroid_location") != null)
					{
						JSONObject dropoffCentroidLocation = (JSONObject) objeto.get("dropoff_centroid_location");
						String tipo = (String)dropoffCentroidLocation.get("type");
						JSONArray prueba = (JSONArray) dropoffCentroidLocation.get("coordinates");
						double latitud =  (double) prueba.get(0);
						double longitud = (double) prueba.get(1);
						dropoffCentroidLocationA.add(latitud);
						dropoffCentroidLocationA.add(longitud);
						dropoffCentroidLocationA.add(tipo);
												
					}
					
					String dropoffCentroidLongitude = (String)objeto.get("dropoff_centroid_longitude");
					int dropoffCommunityArea = -1;
					if(objeto.get("dropoff_community_area") != null)
					{
						dropoffCommunityArea = Integer.parseInt((String)objeto.get("dropoff_community_area"));
					}
					String extras = (String) objeto.get("extras");
					String fare = (String) objeto.get("fare");
					String paymentType = (String) objeto.get("payment_type");
					String pickupCensusTract = (String) objeto.get("pickup_census_tract");
					String pickupCentroidLatitude = (String) objeto.get("pickup_centroid_latitude");
					
					ArrayList pickupCentroidLocation = new ArrayList();
					if(objeto.get("pickup_centroid_location") != null)
					{
						JSONObject pickupCentroidLocation1 = (JSONObject) objeto.get("pickup_centroid_location");
						
					}
					String pickupCentroidLongitude = (String) objeto.get("pickup_centroid_longitude");
					String pickUpCommunityArea = (String) objeto.get("pickup_community_area");
					String taxiId = (String) objeto.get("taxi_id");
					String tips = (String) objeto.get("tips");
					String tolls = (String) objeto.get("tolls");
					String tripEndTimeStamp = (String) objeto.get("trip_end_timestamp");
					String tripId = (String) objeto.get("trip_id");
					double tripMiles = Double.parseDouble((String)objeto.get("trip_miles"));
					int tripSeconds = Integer.parseInt((String)objeto.get("trip_seconds"));
					String tripTimestamp = (String) objeto.get("trip_start_timestamp");
					Double tripTotal = Double.parseDouble((String)objeto.get("trip_total"));
					Service servicioAA�adir = new Service(dropoffCensusTract, dropoffCentroidLatitude, "dropoffCentroidLocation", dropoffCentroidLongitude, dropoffCommunityArea, extras, fare, paymentType, pickupCensusTract, pickupCentroidLatitude, pickupCentroidLongitude, pickUpCommunityArea, taxiId, tips, tolls, tripEndTimeStamp, tripId, tripMiles, tripSeconds, tripTimestamp, tripTotal);
					servicios.a�adir(servicioAA�adir);
					Taxi taxiAA�adir = new Taxi(taxiId, company);

					if(!existeTaxi(taxiAA�adir.getTaxiId()))
					{
						taxis.a�adir(taxiAA�adir);
					}
					iter.next();
					z++;
				}
				System.out.println("Hay "+servicios.darTama�o()+" servicios y "+taxis.darTama�o()+ " Taxis");
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
		}

	@Override
	public ListaDoble<Taxi> getTaxisOfCompany(String company) {
		ListaDoble taxisPorCompa�ia = new ListaDoble();
		if(taxis.darPrimero() != null)
		{
			Node <Taxi> taxi = taxis.darPrimero();
			int numero = taxis.darTama�o();
			while (numero != 0)  
			{
//				System.out.println(company +" "+ taxi.darItem().getCompany());
				
				if(taxi.darItem().getCompany().equals(company))
				{
					taxisPorCompa�ia.a�adir(taxi.darItem());
				}
				taxi = taxi.darSiguiente();
				numero--;
			}
		}
		System.out.println("Inside getTaxisOfCompany with " + company);
		return taxisPorCompa�ia;
	}
	
	public ListaDoble<Service> getTaxiServicesToCommunityArea(int communityArea) {
		ListaDoble serviciosPorAreaComun = new ListaDoble();
			if( servicios.darPrimero() != null )
			{
				Node <Service> servicio = servicios.darPrimero();
				int numero = servicios.darTama�o();
				while  ( numero != 0 )
				{
					if (servicio.darItem().getdropoffCommunityArea() ==communityArea)
					{
						serviciosPorAreaComun.a�adir(servicio.darItem());
					}
					servicio = servicio.darSiguiente();
					numero --;
				}
			}
		
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		return serviciosPorAreaComun;
	}
	public boolean existeTaxi(String id)
	{
		Node<Taxi> nodoTaxi = taxis.darPrimero();
		int numero = taxis.darTama�o();
		while (numero != 0)
		{ 
			if(nodoTaxi.darItem().getTaxiId().equals(id))
			{
				return true;
			}
			numero--;
			nodoTaxi = nodoTaxi.darSiguiente();
		}
		return false;
	}

}
